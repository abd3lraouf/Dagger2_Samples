package com.abdelraouf.design.dagger.sandwich;

import com.abdelraouf.design.dagger.butter.CashewButter;

public class CashewSandwich extends Sandwich{
	private CashewButter mButter;

	public CashewSandwich(CashewButter butter) {
		mButter = butter;
	}

}
