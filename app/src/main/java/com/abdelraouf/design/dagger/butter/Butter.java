package com.abdelraouf.design.dagger.butter;

public abstract class Butter {
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
