package com.abdelraouf.design.dagger.butter;

import dagger.Module;
import dagger.Provides;

@Module
public class ButterModule {
	@ButterScope
	@Provides
	AlmondButter provideAlmondButter() {
		return new AlmondButter();
	}

	@ButterScope
	@Provides
	CashewButter provideCashewButter() {
		return new CashewButter();
	}
}
