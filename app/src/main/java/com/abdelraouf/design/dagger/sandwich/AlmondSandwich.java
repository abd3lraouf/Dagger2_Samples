package com.abdelraouf.design.dagger.sandwich;

import com.abdelraouf.design.dagger.butter.AlmondButter;

public class AlmondSandwich extends Sandwich {
	private AlmondButter mButter;

	public AlmondSandwich(AlmondButter butter) {
		mButter = butter;
	}
}
