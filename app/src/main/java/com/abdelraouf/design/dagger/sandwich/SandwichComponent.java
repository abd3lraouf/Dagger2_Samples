package com.abdelraouf.design.dagger.sandwich;

import com.abdelraouf.design.dagger.HomeActivity;
import com.abdelraouf.design.dagger.butter.ButterComponent;

import dagger.Component;

@SandwichScope
@Component(dependencies = ButterComponent.class, modules = SandwichModule.class)
public interface SandwichComponent {
	void implant(HomeActivity homeActivity);
}
