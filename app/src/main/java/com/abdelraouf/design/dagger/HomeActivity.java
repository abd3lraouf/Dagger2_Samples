package com.abdelraouf.design.dagger;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.abdelraouf.design.dagger.butter.AlmondButter;
import com.abdelraouf.design.dagger.butter.DaggerButterComponent;
import com.abdelraouf.design.dagger.sandwich.AlmondSandwich;
import com.abdelraouf.design.dagger.sandwich.DaggerSandwichComponent;
import com.abdelraouf.design.dagger.sandwich.SandwichComponent;

import javax.inject.Inject;

public class HomeActivity extends AppCompatActivity {
	public static final String TAG = HomeActivity.class.getSimpleName();
	@Inject AlmondButter mButter;
	@Inject AlmondSandwich mSandwich;
	private SandwichComponent mSandwichComponent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		mSandwichComponent = DaggerSandwichComponent
				.builder()
				.butterComponent(DaggerButterComponent.builder().build())
				.build();

		mSandwichComponent.implant(this);

		Log.v(TAG,mButter.toString());
		mSandwich.eat();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSandwichComponent = null;
	}
}
